package dtu.example;

import dtu.ws.fastmoney.*;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.List;

public class SimpleDTUPaySteps {
    String cid, mid;
    SimpleDTUPay dtuPay = new SimpleDTUPay();
    boolean successful;
    BankService bank = new BankServiceService().getBankServicePort();
    User customer = new User();
    User merchant = new User();
    String customerAccountId = "";
    String merchantAccountId = "";

    List<Payment> paymentList;

    @Before
    public void before() throws BankServiceException_Exception {
        List<AccountInfo> accounts = bank.getAccounts();
        for (AccountInfo acc:accounts){
            bank.retireAccount(acc.getAccountId());
            System.out.println(acc.getUser().getFirstName());
        }
    }

    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        Assertions.assertTrue(successful);
    }

    @Given("a customer with a bank account with balance {int}")
    public void aCustomerWithABankAccountWithBalance(int amount) throws BankServiceException_Exception {
        this.customer.setCprNumber("111111");
        this.customer.setFirstName("Ion");
        this.customer.setLastName("Chetraru");
        this.customerAccountId = bank.createAccountWithBalance(this.customer, BigDecimal.valueOf(amount));
    }


    @And("that the customer is registered with DTU Pay")
    public void thatTheCustomerIsRegisteredWithDTUPay() {
        this.cid = dtuPay.registerCustomer(this.customerAccountId);
    }

    @Given("a merchant with a bank account with balance {int}")
    public void aMerchantWithABankAccountWithBalance(int amount) throws BankServiceException_Exception {
        this.merchant.setCprNumber("000000000");
        this.merchant.setFirstName("Abi");
        this.merchant.setLastName("Sotomayor");
        this.merchantAccountId = bank.createAccountWithBalance(this.merchant, BigDecimal.valueOf(amount));
    }

    @And("that the merchant is registered with DTU Pay")
    public void thatTheMerchantIsRegisteredWithDTUPay() {
        this.mid = dtuPay.registerMerchant(this.merchantAccountId);
    }

    @And("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int expected) throws BankServiceException_Exception {
        BigDecimal actual = bank.getAccount(this.customerAccountId).getBalance();
        Assertions.assertEquals(BigDecimal.valueOf(expected),actual);

    }

    @And("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(int expected) throws BankServiceException_Exception {
        BigDecimal actual = bank.getAccount(this.merchantAccountId).getBalance();
        Assertions.assertEquals(BigDecimal.valueOf(expected),actual);
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) {
        successful = dtuPay.pay(amount, this.cid, this.mid);
    }

    @After
    public void after() throws BankServiceException_Exception {
        this.bank.retireAccount(this.customerAccountId);
        this.bank.retireAccount(this.merchantAccountId);
    }



}
