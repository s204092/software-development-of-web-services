package dtu.example;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;

public class SimpleDTUPay {
    Client c = ClientBuilder.newClient();
    WebTarget r = c.target("http://localhost:8080/");

    public boolean pay(int amount, String cid, String mid) {
        Payment payment = new Payment(cid, mid, amount);

        int status = r.path("payments")
                .request()
                .post(Entity.entity(payment, MediaType.APPLICATION_JSON))
                .getStatus();
        return status == Response.Status.CREATED.getStatusCode();
    }

    public List<Payment> getPayments() {
        return r.path("payments")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Payment>>(){});
    }
}
