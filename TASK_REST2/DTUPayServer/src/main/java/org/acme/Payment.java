package org.acme;

public class Payment {

    public String customerID;
    public String merchantID;
    public double amount;

    public Payment(){}
    public Payment(String customerID, String merchantID, double amount){
        this.customerID = customerID;
        this.merchantID = merchantID;
        this.amount = amount;
    }
}
