package org.acme;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PaymentRegistration {

    private static PaymentRegistration pr = null;
    private List<Payment> paymentList = new ArrayList<>();


    public PaymentRegistration() {}

    public static PaymentRegistration getInstance() {
        if (pr == null) {
            pr = new PaymentRegistration();
        }
        return pr;
    }

    public List<Payment> getPayments() {
        return paymentList;
    }

    public void registerPayment(Payment payment) throws Exception {
        if(Objects.equals(payment.customerID, "cid1") && Objects.equals(payment.merchantID, "mid1"))
            paymentList.add(payment);
        else {
            throw new Exception("customer with id " + payment.customerID + " is unknown");
        }

    }


}
