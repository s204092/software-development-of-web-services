package org.acme;


import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Path("/payments")
public class PaymentsResource {
    PaymentRegistration pr = new PaymentRegistration();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> getPayments(){
        return pr.getPayments();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerPayment(Payment payment) throws URISyntaxException, Exception {
        pr.registerPayment(payment);

        return Response.created(new URI("/payments")).build();
    }
}
