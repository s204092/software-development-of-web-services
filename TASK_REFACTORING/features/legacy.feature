Feature: Gilded Rose behaviour of the legacy code

// Example of a scenario
Scenario: Aged Brie increases in quality the older it gets
	Given an item named "Aged Brie" with quality 9 and which has to be sold in 11 days
	When a day has passed
	Then it has to be sold in 10 days
	And its quality is 10

Scenario: The Quality of an item is never negative
	Given an item named "KJKJKfkdskjfkdsjfkjds" with quality 0 and which has to be sold in 11 days
	When a day has passed
	Then its quality is 0

Scenario: The Quality of an item decreases every day
	Given an item named "KJKJKfkdskjfkdsjfkjds" with quality 10 and which has to be sold in 11 days
	When a day has passed
	Then its quality is 9
	
Scenario: "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
	Given an item named "Sulfuras, Hand of Ragnaros" with quality 80 and which has to be sold in 9 days
	When  a day has passed
	Then its quality is 80
	And it has to be sold in 9 days

Scenario: "Backstage passes", like aged brie, increases in Quality as it’s SellIn value approaches
	Given an item named "Backstage passes to a TAFKAL80ETC concert" with quality 10 and which has to be sold in 15 days
	When a day has passed
	Then its quality is 11

Scenario: "Backstage passes", like aged brie, increases in Quality as it’s SellIn value approaches
	Given an item named "Backstage passes to a TAFKAL80ETC concert" with quality 10 and which has to be sold in 9 days
	When a day has passed
	Then its quality is 12

Scenario: "Backstage passes", like aged brie, increases in Quality as it’s SellIn value approaches
	Given an item named "Backstage passes to a TAFKAL80ETC concert" with quality 10 and which has to be sold in 5 days
	When a day has passed
	Then its quality is 13

Scenario: "Backstage passes", like aged brie, increases in Quality as it’s SellIn value approaches
	Given an item named "Backstage passes to a TAFKAL80ETC concert" with quality 10 and which has to be sold in -1 days
	When a day has passed
	Then its quality is 0

Scenario: Once the sell by date has passed, Quality degrades twice as fast
	Given an item named "KJKJKfkdskjfkdsjfkjds" with quality 10 and which has to be sold in -1 days
	When a day has passed
	Then its quality is 8

Scenario: The Quality of an item is never more than 50
	Given an item named "Aged Brie" with quality 50 and which has to be sold in -1 days
	When a day has passed
	Then its quality is 50

Scenario: The Quality of an item is never more than 50
	Given an item named "Aged Brie" with quality 48 and which has to be sold in -6 days
	When a day has passed
	Then its quality is 50
