package dtu.example;

import java.util.ArrayList;
import java.util.List;

public class StringCalculator {

    public StringCalculator() {

    }
    public int add(String numbers) throws Exception {
        if (numbers.isEmpty()){
            return 0;
        }

        char delimiter = ',';

        if (numbers.startsWith("//")){
            delimiter = numbers.charAt(2);
            numbers = numbers.substring(4);
        }

        String[] arrNumbers  = numbers.split("\n|" + delimiter);
        List<Integer> negatives = new ArrayList<>();


        int sum = 0;
        for (String numString:arrNumbers){
            int num = Integer.parseInt(numString);
            if (num < 0){
                negatives.add(num);
            }
            sum += num;
        }

        if (!negatives.isEmpty()){
            throw new Exception("Negatives not allowed " + negatives.toString());
        }
        return sum;
    }
}
