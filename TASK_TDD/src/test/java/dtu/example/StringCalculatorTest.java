package dtu.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {
    @Test
    public void testEmptyStringCalculator() throws Exception {
        var sc = new StringCalculator();

        Assertions.assertEquals(0,sc.add(""),"This should be 0");
    }

    @Test
    public void testOneStringCalculator() throws Exception {
        var sc = new StringCalculator();

        Assertions.assertEquals(1,sc.add("1"),"This should be 1");
    }

    @Test
    public void testTwoStringCalculator() throws Exception {
        var sc = new StringCalculator();

        Assertions.assertEquals(3,sc.add("1,2"),"This should be 3");
    }

    @Test
    public void testMoreStringCalculator() throws Exception {
        var sc = new StringCalculator();

        int size = (int) (Math.random()*100);
        String numbers = "1" + ",1".repeat(Math.max(0, size - 1));

        Assertions.assertEquals(size,sc.add(numbers));
    }

    @Test
    public void testWithDelimiterStringCalculator() throws Exception {
        var sc = new StringCalculator();
        Assertions.assertEquals(6,sc.add("1\n2,3"));
    }

    @Test
    public void testWithCustomDelimiterStringCalculator() throws Exception {
        var sc = new StringCalculator();
        Assertions.assertEquals(6,sc.add("//;\n1;2;3"));
    }
    @Test
    public void testNegativesStringCalculator() throws Exception {
        var sc = new StringCalculator();
        Exception thrown = Assertions.assertThrows(Exception.class , () ->  sc.add("-1,1,-3"));

        String expectedMessage = "Negatives not allowed [-1, -3]";
        String actualMessage = thrown.getMessage();

        Assertions.assertEquals(expectedMessage, actualMessage);
    }
}
