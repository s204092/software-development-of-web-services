package dtu.example;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class StringCalculationSteps {
    String input = "";
    int sum;
    StringCalculator sc =  new StringCalculator();

    @Given("that input string is empty")
    public void that_input_string_is_empty() {
        input = "";
    }

    @When("add method is called")
    public void add_method_is_called() throws Exception {
        sum = sc.add(input);

    }

    @Then("the return should be {int}")
    public void the_return_should_be(Integer int1) {
        Assertions.assertEquals(int1, sum);
    }


    @Given("that input string is {string}")
    public void thatInputStringIs(String numbers) {
        input = numbers;
    }

}
