package dtu.example;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class JenkinsTesterSteps {

    String input = "";
    String res ="";

    JenkinsTester jt =  new JenkinsTester();
    @Given("that the input string is {string}")
    public void thatInputStringIs(String name) {
        input = name;
    }

    @When("JenkinsTest method is run")
    public void JenkinsTestMethodIsRun(){
        res = jt.JenkinsTest(input);

    }

    @Then("the return should be {string}")
    public void TheReturnShouldBe(String string) {
        Assertions.assertEquals(string, res);
    }

}
