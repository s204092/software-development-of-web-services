package dtu.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JenkinsTesterTest {
    @Test
    public void testEmptyString(){
        var jt = new JenkinsTester();
        Assertions.assertEquals("Hello ",jt.JenkinsTest(""));
    }

    @Test
    public void testJohn() {
        var jt = new JenkinsTester();
        Assertions.assertEquals("Hello John", jt.JenkinsTest("John"));
    }
}
