Feature: JenkinsTester
  Returns a string

  Scenario: Return string Hello John
    Given that the input string is "John"
    When JenkinsTest method is run
    Then the return should be "Hello John"