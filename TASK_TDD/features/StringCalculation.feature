Feature: StringCalculation
  Adds the numbers in a string

  Scenario: Return 0 when the input string is empty
    Given that input string is empty
    When add method is called
    Then the return should be 0

  Scenario: Return the number when there is only one number in the input string
    Given that input string is "1"
    When add method is called
    Then the return should be 1

