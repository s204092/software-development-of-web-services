﻿Tasks and Assignments Course 02267

Hubert Baumeister January 2024

1 Task (single/in pairs/using mob programming): TDD and BDD using a coding kata task\_tdd

1. Introduction to the concept coding katas

In martial arts, a kata is a well-defined sequence of steps to be performed by a person to train attack and defense movements that he/she can later use in fights. Katas are a well integrated part of the training and are performed every training [^1][.](#_page0_x73.28_y752.32)

In software development, in particular agile software developement, a coding kata is a usually short, well-defined problem to train a certain technique, like test-driven-development. Like the martial arts kata, the idea is to solve the same problem regularly to train the respective coding techniques. The solutions of a kata can be modified to train different aspects, for example using different testing frameworks (e.g. JUnit and Cucumber) or different programming languages. In our case, we use the String Calculator kata [^2][ ](#_page0_x73.28_y763.28)from Roy Osherrove. The idea is, that once you know the basic steps of TDD and refactoring, you can apply these techniques without much thinking in real projects.

If you already know the String Calculator TDD kata, choose one of the TDD katas available at Kata-log[ .](#_page0_x73.28_y774.24)[^3]

2. Implement the String Calculator TDD Kata using TDD and JUnit
1. Before you start:
- Try not to read ahead in the requirements; take one requirement after the other. However, there are Tips at the end of the document that help you with the setup which you should read first :-)
- Do one task at a time. The trick is to learn to work incrementally.
- Make sure you only test for correct inputs. There is no need to test for invalid inputs for this kata
2. String Calculator

1\. Create a simple StringCalculator class with an instance method int add(string numbers) , e.g.

var sc = new StringCalculator(); assertEquals(5,sc.add("2,3"));

- The method can take 0, 1 or 2 numbers separated by comma, and will return their sum (for an empty string it will return 0) for example “” or “1” or “1,2” result in "0", "1", and "3", respectively.
- Start with the simplest test case of an empty string and move to 1 and two numbers
- Remember to solve things as simply as possible so that you force yourself to write tests you did not think about
- Remember to refactor after each passing test
1. Allow the add method to handle an unknown amount of numbers
1. Allow the add method to handle new lines between numbers (instead of commas).
- the following input is ok: "1\n2,3" (will equal 6), where \n is a new line.
- the following input is NOT ok: “1,\n” (no need to test it - just clarifying)
3. Support different delimiters
- to change a delimiter, the beginning of the string will contain a separate line that looks like this: “//[delimiter]\n[numbers...]” for example “//;\n1;2” should return three where the default delimiter is ‘;’ .
- the first line is optional if the delimiter is , (comma). All existing scenarios should still be supported
4. Calling add with a negative number will throw an exception “negatives not allowed” - and the negative that was passed. If there are multiple negatives, show all of them in the exception message
3. Implement the String Calculator TDD Kata using TDD and Cucumber

Start with the String Calculator TDD Kata again from scratch (i.e. in a fresh project) and use Cucumber as the testing framework.

If you have time left, repeat both versions of the kata with a different partner. Note that the power      (and idea behind) kata’s is the repetition. You can also choose another TDD kata from Kata-log [3](#_page0_x73.28_y774.24).

The goal of this task is to familiarize yourself with behaviour/test-driven development (BDD/TDD) using Cucumber and JUnit 5. This will be the way you are going to develop your services.

If you already know about BDD and TDD, it still makes sense to do the katas to refresh your knowlege and train with the frameworks used in this course.

4. Tips
1. <a name="_page1_x56.69_y470.58"></a>Maven setup

Later, when we use build automation, we need to be able to build and run the tests from the command line in addition to using an IDE. A build system that I recommend is Maven . It is easy to create a Maven project, and when you run mvn test , it will automatically build and run JUnit and Cucumber tests (if present). Other build systems are also possible, but I have the most experience with Maven

and cannot help you with other build systems.

Maven is also supported by Eclipse and IntelliJ. You can either

1. directly create a Maven project in Eclipse (here you should create a simple project and skip the archetype selection unless you know what your are doing)
1. you can create a normal Eclipse project and then convert it to a Maven project.
1. Finally, you can create a Maven project directly on the command line or use an existing project and then import the project as a Maven project into Eclipse.

A Maven project is controlled by its pom.xml file. In this file you have to add the libraries that you want to use as dependencies. Usually you can find the right dependencies when you search for the framework you want to include together with the keyword maven repository in Google.

You should also make sure that you specifiy the Java version in your pom.xml file. In the example below, it is Java version 11.

<properties>

<maven.compiler.source>11</maven.compiler.source>

<maven.compiler.target>11</maven.compiler.target> </properties>

The default compiler version of Maven (Java 5) is not supported anymore.

I have uploaded a small Cucumber example [^4][ ](#_page2_x73.28_y741.34)as a Maven project which you can use as a starting point for both of your Katas. The Maven project contains all required dependencies to work with Cucumber and JUnit 5.

2. Cucumber with JUnit 5 (aka Jupiter)

The easiest way to start with Cucumber is to download the CucumberExample project [4](#_page2_x73.28_y741.34), which contains a small, working Cucumber test example. When you use that project to start with, please remove all the parts that you don’t need or rename them so that they fit with the purpose of your project.

Note that Cucumber works differently for JUnit 5 than JUnit 4. And there are not yet that many Cucumber setup examples for Cucumber and JUnit 5. However, using the junit-vintage dependency in the pom file will provide the old behaviour of Cucumber also to JUnit 5.

The example project is setup, so that it can work both, with JUnit 4 and JUnit 5 tests and JUnit 4 and JUnit 5 test integration. Please let me know if you have any problems with that project.

If you have questions regarding how to work with Cucumber in Java and looking for examples on how to use Cucumber with Java, then "The Cucumber for Java Book" [^5][ ](#_page2_x73.28_y752.30)gives you all the answers (and better answers than Google).

Here is also an example project [^6][ ](#_page2_x73.28_y763.26)from the course Software Engineering I using Cucumber, and a video[^7][ ](#_page2_x73.28_y774.22)showing Cucumber techniques. Note that the project and the video use Cucumber together

with JUnit 4.x.

Make sure that Test or Tests appears in the name of the class that has the @Cucumberannotation in the firstposition or in the last position. For example, CucumberTest will work, but CucumberTestsTwo won’t be recognized by Maven (it will be recognized by Eclispe though). So, if Maven does not run your Cucumber tests, please check that the name of the test class is correct.

3. Dealing with multiple lines in Cucumber

If you use \n in strings in scenarios, this will be interpreted by Cucumber as two characters \ and n instead of the newline character. The solution is to replace \n in the input spring by new line before passing it on to the function under test. For example, you can use

input.replace("\\n","\n");

which returns a string where \n is being replaced by the newline character.
4

[^1]: <a name="_page0_x73.28_y752.32"></a>Example of a kata in Karate <https://www.youtube.com/watch?v=I9MHie6EyYA&ab_channel=BushidoTamashi>
[^2]: <a name="_page0_x73.28_y763.28"></a><http://osherove.com/tdd-kata-1/>
[^3]: <a name="_page0_x73.28_y774.24"></a><https://kata-log.rocks/tdd>
[^4]: <a name="_page2_x73.28_y741.34"></a><http://www2.imm.dtu.dk/courses/02267/files/CucumberExample.zip>
[^5]: <a name="_page2_x73.28_y752.30"></a><https://findit.dtu.dk/en/catalog/2447040024>
[^6]: <a name="_page2_x73.28_y763.26"></a><http://www2.imm.dtu.dk/courses/02161/2018/files/library01_solution.zip>
[^7]: <a name="_page2_x73.28_y774.22"></a><https://www.youtube.com/watch?v=sPHDyp8ox6Y>