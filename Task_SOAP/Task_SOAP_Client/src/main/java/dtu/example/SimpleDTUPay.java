package dtu.example;

import jakarta.ws.rs.*;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;

public class SimpleDTUPay {
    Client c = ClientBuilder.newClient();
    WebTarget r = c.target("http://localhost:8080/");

    public boolean pay(int amount, String cid, String mid) throws PaymentException {
        Payment payment = new Payment(cid, mid, amount);

        Response response = r.path("payments")
                .request()
                .post(Entity.entity(payment, MediaType.APPLICATION_JSON));

        int status = response.getStatus();

        if(status == Response.Status.BAD_REQUEST.getStatusCode()){
            String error_msg = response.readEntity(String.class);
            throw new PaymentException(error_msg);
        }

        return status == Response.Status.CREATED.getStatusCode();
    }

    public List<Payment> getPayments() {
        return r.path("payments")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Payment>>(){});
    }


    public String registerCustomer(String customerAccountID ){
        dtu.example.Client client = new dtu.example.Client(customerAccountID);
        return r.path("customers").request().post(Entity.entity(client, MediaType.APPLICATION_JSON),String.class);
    }

    public String registerMerchant(String merchantAccountID ){
        dtu.example.Client client = new dtu.example.Client(merchantAccountID);
        return r.path("customers").request().post(Entity.entity(client, MediaType.APPLICATION_JSON),String.class);
    }


    public String getPayment(String paymentID) throws PaymentException {
        try {
            return r.path("payments/").path(paymentID)
                    .request()
                    .get(String.class);
        } catch (BadRequestException e) {
            throw new PaymentException(e.getResponse().readEntity(String.class));
        }

    }
}
