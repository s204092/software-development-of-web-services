package dtu.example;

import java.util.Objects;

public class Payment {

    public String customerID;
    public String merchantID;
    public String paymentID;
    public int amount;

    public Payment(){}
    public Payment(String customerID, String merchantID, int amount){
        this.customerID = customerID;
        this.merchantID = merchantID;
        this.amount = amount;
        this.paymentID = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Double.compare(amount, payment.amount) == 0 && Objects.equals(customerID, payment.customerID) && Objects.equals(merchantID, payment.merchantID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID, merchantID, amount);
    }
}
