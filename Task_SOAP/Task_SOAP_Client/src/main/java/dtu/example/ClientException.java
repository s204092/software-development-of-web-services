package dtu.example;

public class ClientException extends Exception {
    public ClientException(String message ){
        super(message);
    }
    public ClientException(){}
}
