package dtu.example;

import java.util.UUID;

public class Client {
    public String accountID;
    public String id;

    public Client(String accountID) {
        this.accountID = accountID;
    }

    public Client() {}

    public String getId() {
        return this.id;
    }
}
