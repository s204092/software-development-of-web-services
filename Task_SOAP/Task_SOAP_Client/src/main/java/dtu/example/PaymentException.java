package dtu.example;


public class PaymentException extends Exception {
    public PaymentException(String message){
        super(message);
    }
    public PaymentException(){}
}
