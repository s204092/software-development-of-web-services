package dtu.example;

import dtu.ws.fastmoney.*;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import jakarta.ws.rs.BadRequestException;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.List;

public class SimpleDTUPaySteps {
    String cid, mid;
    SimpleDTUPay dtuPay = new SimpleDTUPay();
    boolean successful;
    BankService bank = new BankServiceService().getBankServicePort();
    User customer = new User();
    User merchant = new User();
    String customerAccountId = "";
    String merchantAccountId = "";
    String errorMessage;

    //List<Payment> paymentList;
    private String paymentID;

    @Before
    public void before() throws BankServiceException_Exception {
        List<AccountInfo> accounts = bank.getAccounts();
        for (AccountInfo acc:accounts){
                bank.retireAccount(acc.getAccountId());
                System.out.println(acc.getUser().getFirstName());
        }
    }

    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        Assertions.assertTrue(successful);
    }

    @Given("a customer with a bank account with balance {int}")
    public void aCustomerWithABankAccountWithBalance(int amount) throws BankServiceException_Exception {
        this.customer.setCprNumber("111111");
        this.customer.setFirstName("Ion");
        this.customer.setLastName("Chetraru");
        this.customerAccountId = bank.createAccountWithBalance(this.customer, BigDecimal.valueOf(amount));
    }

    @And("that the customer is registered with DTU Pay")
    public void thatTheCustomerIsRegisteredWithDTUPay() {
        this.cid = dtuPay.registerCustomer(this.customerAccountId);
    }

    @And("that the customer is not registered with DTU Pay")
    public void thatTheCustomersNotRegisteredWithDTUPay() {
        this.customerAccountId = "";
        this.cid = "";
    }

    @Given("a merchant with a bank account with balance {int}")
    public void aMerchantWithABankAccountWithBalance(int amount) throws BankServiceException_Exception {
        this.merchant.setCprNumber("000000000");
        this.merchant.setFirstName("Abi");
        this.merchant.setLastName("Sotomayor");
        this.merchantAccountId = bank.createAccountWithBalance(this.merchant, BigDecimal.valueOf(amount));
    }

    @And("that the merchant is registered with DTU Pay")
    public void thatTheMerchantIsRegisteredWithDTUPay() {
        this.mid = dtuPay.registerMerchant(this.merchantAccountId);
    }

    @And("that the merchant is not registered with DTU Pay")
    public void thatTheMerchantIsNotRegisteredWithDTUPay() {
        this.merchantAccountId = "";
        this.mid = "";
    }

    @And("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int expected) throws BankServiceException_Exception {
        BigDecimal actual = bank.getAccount(this.customerAccountId).getBalance();
        Assertions.assertEquals(BigDecimal.valueOf(expected), actual);
    }

    @And("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(int expected) throws BankServiceException_Exception {
        BigDecimal actual = bank.getAccount(this.merchantAccountId).getBalance();
        Assertions.assertEquals(BigDecimal.valueOf(expected), actual);
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) {
        try {
            successful = dtuPay.pay(amount, this.cid, this.mid);
        } catch (BadRequestException | PaymentException e) {
            errorMessage = e.getMessage();
        }
    }

    @Given("a payment id {string}")
    public void aPaymentId(String paymentID) {
        this.paymentID = paymentID;
    }

    @And("the payment does not exist")
    public void thePaymentDoesNotExist() {
        List<Payment> payments = dtuPay.getPayments();
        Payment shouldBeNull = payments.stream().filter(p -> p.paymentID.equals(this.paymentID)).findFirst().orElse(null);
        Assertions.assertNull(shouldBeNull);
    }

    @When("the payment is accessed an error with message {string} is shown")
    public void thePaymentIsAccessedAnErrorWithMessageIsShown(String string) {
        try {
            dtuPay.getPayment(this.paymentID);
        }  catch(PaymentException e) {
            Assertions.assertEquals(string,e.getMessage());
        }
    }


    @Then("the payment is not successful")
    public void thePaymentIsNotSuccessful() {
        Assertions.assertFalse(successful);
    }
    @Then("an error message is returned saying {string}")
    public void anErrorMessageIsReturnedSaying(String string) {
        Assertions.assertEquals(string,errorMessage);
    }
    @After
    public void after() throws BankServiceException_Exception {
        try{
            this.bank.retireAccount(this.customerAccountId);
            this.bank.retireAccount(this.merchantAccountId);}
        catch(Exception e){

        }
    }

}
